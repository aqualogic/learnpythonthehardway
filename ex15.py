from sys import argv

script, filename = argv  #read arguments from command line and assign it the variables script and filename:

txt = open(filename) # open file named the filename stored in variable 'filename'

print "Here's your file %r:" % filename   # print the name of the file as entered while running the command at command line
print txt.readline() # read data from the file and print it on standard output

txt.close()

'''
print "Type the filename again:"
file_again = raw_input(">>")  # read the data entered and assign it to new variable file_again.
                              # Through this we are requesting the user to re-enter the filename

txt_again = open(file_again) # open the file with filename read above

print txt_again.read() # read and print the file opened above
'''

'''
 For those who are comfortable with command-line and prefer typing in big 
 commands, for them it is better to write a script for their own use that
 accepts the filename as part of the arguments. For it is important that
 user knows what the script expects.

 When the user is non-technical and who is not so comfortable with writing
 big commands on the command-line, for them it is better that the script 
 written is kind of interactive and here is where raw_input comes into 
 picture.
'''
